import unittest
from needle_and_haystack.src.find_needle import Solution

class SolveNeedle(unittest.TestCase):

    def test_first(self):
        res = Solution().find_needle("sadbutsad","sad")
        self.assertEqual(res, 0)

    def test_last(self):
        res = Solution().find_needle("butsad","sad")
        self.assertEqual(res, 3)

    def test_empty_needle(self):
        res = Solution().find_needle("sadbutsad","")
        self.assertEqual(res, 0)

    def test_empty_haystack(self):
        res = Solution().find_needle("","notempty")
        self.assertEqual(res, -1)

    def test_not_include(self):
        res = Solution().find_needle("saysomething","no")
        self.assertEqual(res, -1)
    

